       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CALGRADE.
       AUTHOR. THANAWAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION.
       FD  GRADE-FILE.
       01 GRADE-DETAILS.
          88 END-OF-GRADE-FILE                VALUE HIGH-VALUE.
       05 SUBJECT-ID            PIC 9(6).
          05 SUBJECT-NAME       PIC X(50).
          05 CREDIT             PIC 9.
          05 GRADE              PIC 9(2).
       FD  AVG-FILE.
       01 AVG                   PIC 9(2)V9(3) VALUE ZEROS.
       01 AVG-SCI-DETAIL        PIC 9(2)V9(3) VALUE ZEROS.
       01 AVG-CS-DETAIL         PIC 9(2)V9(3) VALUE ZEROS.
       WORKING-STORAGE SECTION. 
       01 GRADE-TO-NUM          PIC 9V9.
       01 SUM-CREDIT            PIC 9(3)      VALUE ZEROS.
       01 SUM-GRADE-CREDIT      PIC 9(3)V9(2) VALUE ZEROS.
       01 SUM-CREDIT-SCI        PIC 9(3)      VALUE ZEROS.
       01 SUM-GRADE-CREDIT-SCI  PIC 9(3)V9(2) VALUE ZEROS.
       01 SUM-CREDIT-CS         PIC 9(3)      VALUE ZEROS.
       01 SUM-GRADE-CREDIT-CS   PIC 9(3)V9(2) VALUE ZEROS.
       01 RESULT.         
          05 AVG-GRADE          PIC 9(2)V9(3) VALUE ZEROS.
          05 AVG-SCI-GRADE      PIC 9(2)V9(3) VALUE ZEROS.
          05 AVG-CS-GRADE       PIC 9(2)V9(3) VALUE ZEROS.
       PROCEDURE DIVISION.
       001-BEGIN.
           OPEN INPUT GRADE-FILE 

           PERFORM UNTIL END-OF-GRADE-FILE 
                   READ GRADE-FILE 
                   AT END
                      SET END-OF-GRADE-FILE TO TRUE
                   END-READ
                   IF NOT END-OF-GRADE-FILE THEN
                      PERFORM 001-PROCESS
                   END-IF

           END-PERFORM
           COMPUTE AVG-GRADE = SUM-GRADE-CREDIT / SUM-CREDIT 
           DISPLAY "AVG-GRADE = " AVG-GRADE
           COMPUTE AVG-SCI-GRADE = SUM-GRADE-CREDIT-SCI / SUM-CREDIT-SCI
           DISPLAY "AVG-SCI-GRADE = " AVG-SCI-GRADE
           COMPUTE AVG-CS-GRADE = SUM-GRADE-CREDIT-CS / SUM-CREDIT-CS 
           DISPLAY "AVG-CS-GRADE = " AVG-CS-GRADE 

           CLOSE GRADE-FILE
           PERFORM 002-WRITE-FILE THRU 002-EXIT
           GOBACK
           .
       
       001-PROCESS.
           EVALUATE TRUE 
           WHEN GRADE = "A"
                MOVE 4 TO GRADE-TO-NUM 
           WHEN GRADE = "B+"
                MOVE 3.5 TO GRADE-TO-NUM
           WHEN GRADE = "B"
                MOVE 3 TO GRADE-TO-NUM
           WHEN GRADE = "C+"
                MOVE 2.5 TO GRADE-TO-NUM
           WHEN GRADE = "C"
                MOVE 2 TO GRADE-TO-NUM
           WHEN GRADE = "D+"
                MOVE 1.5 TO GRADE-TO-NUM 
           WHEN GRADE = "D"
                MOVE 1 TO GRADE-TO-NUM
           WHEN OTHER
                MOVE 0 TO GRADE-TO-NUM
           END-EVALUATE
           COMPUTE SUM-GRADE-CREDIT = SUM-GRADE-CREDIT +
              (CREDIT * GRADE-TO-NUM)
           COMPUTE SUM-CREDIT = SUM-CREDIT + CREDIT 
           IF SUBJECT-ID(1:1) = 3 THEN
              COMPUTE SUM-GRADE-CREDIT-SCI =
                 SUM-GRADE-CREDIT-SCI +(CREDIT * GRADE-TO-NUM)
              COMPUTE SUM-CREDIT-SCI = SUM-CREDIT-SCI + CREDIT 
           END-IF 
           IF SUBJECT-ID(1:2) = 31 THEN
              COMPUTE SUM-GRADE-CREDIT-CS = SUM-GRADE-CREDIT-CS
                 +(CREDIT * GRADE-TO-NUM)
              COMPUTE SUM-CREDIT-CS = SUM-CREDIT-CS + CREDIT 
           END-IF
           .
       002-WRITE-FILE.
           OPEN OUTPUT AVG-FILE
           MOVE AVG-GRADE TO AVG 
           WRITE AVG
           MOVE AVG-SCI-GRADE TO AVG-SCI-DETAIL
           WRITE AVG-SCI-DETAIL
           MOVE AVG-CS-GRADE TO AVG-CS-DETAIL
           WRITE AVG-CS-DETAIL
           CLOSE AVG-FILE
           .
       001-EXIT.
       002-EXIT.
           EXIT.